from django.apps import AppConfig


class AdminPortalConfig(AppConfig):
    name = 'tesserae.portal_apps.admin_portal'
