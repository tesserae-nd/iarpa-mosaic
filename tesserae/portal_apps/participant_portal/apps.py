from django.apps import AppConfig


class ParticipantPortalConfig(AppConfig):
    name = 'tesserae.portal_apps.participant_portal'
