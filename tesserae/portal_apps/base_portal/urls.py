from django.conf.urls import url
from django.views.generic.base import TemplateView

urlpatterns = [
url(r'^participant/new-issue$',
    TemplateView.as_view(template_name='pages/test-datatables.html'), name='new_issue'),
url(r'^participant/my-issues$',
    TemplateView.as_view(template_name='pages/participant/issues/view_issues_list.html'), name='my_issues_list'),
]
