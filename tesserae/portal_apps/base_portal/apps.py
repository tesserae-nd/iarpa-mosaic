from django.apps import AppConfig


class PortalConfig(AppConfig):
    name = 'tesserae.portal_apps.base_portal'
